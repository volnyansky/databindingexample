package kultprosvet.com.databindingexample;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import kultprosvet.com.databindingexample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private Calculator calc;
    public final ObservableField<Boolean> fieldEnabled=new ObservableField<>();
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fieldEnabled.set(true);
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main);
        calc=new Calculator(this);
        binding.setCalc(calc);
        binding.setActivity(this);
        user=new User();
        binding.setUser(user);
        calc.calculate("1");
        calc.calculate("2");
        calc.calculate("+");
        calc.calculate("2");
        calc.calculate("=");
        Log.d("calc",calc.getScreenResult());

        //setContentView(R.layout.activity_main);
    }
    public void clickAnother(){
        Log.d("calc","another ");
        calc.calculate("4");
        calc.calculate("5");
        calc.calculate("+");
        calc.calculate("5");
        calc.calculate("=");
        Log.d("calc",calc.getScreenResult());
        boolean v=!fieldEnabled.get();
        fieldEnabled.set(v);
        user.setLastname("Dupkin");
    }
    public View.OnClickListener onClick=new View.OnClickListener(){
        @Override
        public void onClick(View v) {

        }
    };
}

package kultprosvet.com.databindingexample;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by Stanislav Volnyansky on 05.07.16.
 */
public class User extends BaseObservable{
    private String lastname="Pupkin";
    private String firstName="Vasya";
    @Bindable
    public String getLastname() {
        return lastname;
    }

    public User setLastname(String lastname) {
        this.lastname = lastname;
        notifyPropertyChanged(kultprosvet.com.databindingexample.BR.lastname);
        notifyPropertyChanged(kultprosvet.com.databindingexample.BR.isvalid);
        return this;
    }
    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(kultprosvet.com.databindingexample.BR.firstName);
        notifyPropertyChanged(kultprosvet.com.databindingexample.BR.isvalid);
        return this;
    }
    @Bindable
    boolean getIsvalid(){
        return lastname!=null && firstName!=null
                && !lastname.equals("") && !firstName.equals("");
    }
}
